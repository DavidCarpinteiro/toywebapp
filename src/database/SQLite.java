package database;

import java.sql.*;

public class SQLite {
    private static final String DATABASE_FILE = "sqlite/db/data.db";
    private static final String url = "jdbc:sqlite:" + DATABASE_FILE;

    public SQLite() {
        createNewDatabase();
        dropTable();
        createNewTable();
    }

    /*
    public static void main(String[] args) {
        SQLite sq = new SQLite();
        sq.getAccount("david2");
        System.out.println("Here");
        sq.dropTable();
        sq.createNewTable();
        sq.insertAccount("david", "1234", true, false);
        sq.insertAccount("david2", "12345", true, true);

        sq.getAccount("david");
        sq.deleteAccount("david");
        Values v = sq.getAccount("david");

        sq.update("david2", "0000", false, false);
        sq.getAccount("david2");
    }
     */

    public void update(String username, String passsword, boolean login, boolean locked) {
        String sql = "UPDATE accounts SET password = ? , "
                + "login = ? , "
                + "locked = ? "
                + "WHERE username = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, passsword);
            pstmt.setInt(2, login ? 1 : 0);
            pstmt.setInt(3, locked ? 1 : 0);
            pstmt.setString(4, username);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteAccount(String username) {
        String sql = "DELETE FROM accounts WHERE username = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, username);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Values getAccount(String username) {
        String sql = "SELECT DISTINCT * FROM accounts where username = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, username);

            ResultSet rs = pstmt.executeQuery();

            return new Values(rs.getString("username"),
                    rs.getString("password"),
                    rs.getInt("login"),
                    rs.getInt("locked"));


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public void insertAccount(String username, String password, boolean login, boolean locked) {
        String sql = "INSERT INTO accounts(username,password,login,locked) VALUES(?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setInt(3, login ? 1 : 0);
            pstmt.setInt(4, locked ? 1 : 0);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createNewTable() {

        String sql = "CREATE TABLE IF NOT EXISTS accounts ("
                + " username text PRIMARY KEY,"
                + " password text NOT NULL,"
                + " login integer,"
                + " locked integer"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {

            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void dropTable() {
        String sql = "DROP TABLE IF EXISTS accounts";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } /*finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }*/

        return conn;
    }

    private void createNewDatabase() {

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public class Values {
        public String username, password;
        public boolean login, locked;

        Values(String username, String password, int login, int locked) {
            this.username = username;
            this.password = password;
            this.login = login == 1;
            this.locked = locked == 1;
        }

    }


    /*





    public void selectAll(){
        String sql = "SELECT id, name, capacity FROM warehouses";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" +
                        rs.getString("name") + "\t" +
                        rs.getDouble("capacity"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    public void addInventory(String material, int warehouseId, double qty) {
        // SQL for creating a new material
        String sqlMaterial = "INSERT INTO materials(description) VALUES(?)";

        // SQL for posting inventory
        String sqlInventory = "INSERT INTO inventory(warehouse_id,material_id,qty)"
                + "VALUES(?,?,?)";

        ResultSet rs = null;
        Connection conn = null;
        PreparedStatement pstmt1 = null, pstmt2 = null;

        try {
            // connect to the database
            conn = this.connect();
            if(conn == null)
                return;

            // set auto-commit mode to false
            conn.setAutoCommit(false);

            // 1. insertAccount a new material
            pstmt1 = conn.prepareStatement(sqlMaterial,
                    Statement.RETURN_GENERATED_KEYS);

            pstmt1.setString(1, material);
            int rowAffected = pstmt1.executeUpdate();

            // get the material id
            rs = pstmt1.getGeneratedKeys();
            int materialId = 0;
            if (rs.next()) {
                materialId = rs.getInt(1);
            }

            if (rowAffected != 1) {
                conn.rollback();
            }
            // 2. insertAccount the inventory
            pstmt2 = conn.prepareStatement(sqlInventory);
            pstmt2.setInt(1, warehouseId);
            pstmt2.setInt(2, materialId);
            pstmt2.setDouble(3, qty);
            //
            pstmt2.executeUpdate();
            // commit work
            conn.commit();

        } catch (SQLException e1) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                System.out.println(e2.getMessage());
            }
            System.out.println(e1.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt1 != null) {
                    pstmt1.close();
                }
                if (pstmt2 != null) {
                    pstmt2.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e3) {
                System.out.println(e3.getMessage());
            }
        }
    }
    */


}
