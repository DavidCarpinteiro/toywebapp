package interfaces;

import exceptions.*;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import tools.Storage;
import user.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Key;

public interface IAuthenticator {

    static void create_account(String name, String pwd1, String pwd2) throws PasswordsDoNotMatch, UsernameAlreadyExists {

        if (Storage.hasAccount(name)) {
            throw new UsernameAlreadyExists();
        }

        if (!pwd1.equals(pwd2)) {
            throw new PasswordsDoNotMatch();
        }

        IAccount account = new Account(name, pwd1);

        Storage.saveAccount(account);
    }


    static void delete_account(String name) throws AccountLogin, AccountNotLocked, AccountNotFound {

        IAccount account = Storage.getAccount(name);

        if (!account.isLocked()) {
            throw new AccountNotLocked();
        }

        if (account.isLogin()) {
            throw new AccountLogin();
        }

        Storage.deleteAccount(name);
    }


    static IAccount get_account(String name) throws AccountNotFound {

        IAccount account = Storage.getAccount(name);

        return account.convert();
    }


    static void change_pwd(String name, String pwd1, String pwd2) throws PasswordsDoNotMatch, AccountNotFound {

        if (!pwd1.equals(pwd2)) {
            throw new PasswordsDoNotMatch();
        }

        IAccount account = Storage.getAccount(name);

        account.setPassword(pwd1);

        Storage.updateAccount(account);
    }


    static IAccount login(String name, String pwd) throws AccountNotFound, AccountLocked, AuthenticationError {

        IAccount account = Storage.getAccount(name);

        if (account.isLocked()) {
            throw new AccountLocked();
        }

        if (account.checkPassword(pwd)) {
            throw new AuthenticationError();
        }

        account.setLogin();

        Storage.updateAccount(account);

        return account;
    }


    static void logout(IAccount acc) {

        acc.setLogout();

        Storage.updateAccount(acc);

    }


    static IAccount login(HttpServletRequest req, HttpServletResponse res) {

        // Creating JWT
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();

        // Parsing JWT
        try {

            Jwts.parser().setSigningKey(key).parseClaimsJws(jws);

        } catch (JwtException e) {
            //don't trust the JWT!
        }

        try {
            String name = req.getParameter("username");
            String pwd = req.getParameter("password");
            HttpSession session = req.getSession(true);
            IAccount authUser = login(name, pwd);

            //TODO
            session.setAttribute("JWT", jws);

            // continue with authenticated user (redirect?)

        } catch (AccountNotFound | AccountLocked | AuthenticationError e) {
            e.printStackTrace();
        }

        return null;
    }
}
