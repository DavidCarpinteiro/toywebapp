package interfaces;

import java.util.Date;

public interface IAccount {

    String getUsername();

    String getPassword();

    void setPassword(String password);

    long getID();

    boolean isLocked();

    void setLocked(boolean locked);

    boolean isLogin();

    IAccount convert();

    void setLogin();

    void setLogout();

    Date getCreation();

    Date getModified();

    boolean checkPassword(String password);
}
