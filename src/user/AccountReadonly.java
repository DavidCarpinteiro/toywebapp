package user;

import interfaces.IAccount;

import java.util.Date;
import java.util.Objects;

public class AccountReadonly implements IAccount {

    private final IAccount instance;

    public AccountReadonly(IAccount account) {
        Objects.requireNonNull(account, "Account can't be NULL!");
        this.instance = account;
    }

    @Override
    public String getUsername() {
        return instance.getUsername();
    }

    @Override
    public String getPassword() {
        return instance.getPassword();
    }

    @Override
    public void setPassword(String password) {
        throw new IllegalStateException();
    }

    @Override
    public long getID() {
        return instance.getID();
    }

    @Override
    public boolean isLocked() {
        return instance.isLocked();
    }

    @Override
    public void setLocked(boolean locked) {
        throw new IllegalStateException();
    }

    @Override
    public boolean isLogin() {
        return instance.isLogin();
    }

    @Override
    public IAccount convert() {
        throw new IllegalStateException();
    }

    @Override
    public void setLogin() {
        throw new IllegalStateException();
    }

    @Override
    public void setLogout() {
        throw new IllegalStateException();
    }

    @Override
    public Date getCreation() {
        return instance.getCreation();
    }

    @Override
    public Date getModified() {
        return instance.getModified();
    }

    @Override
    public boolean checkPassword(String password) {
        return instance.checkPassword(password);
    }


}
