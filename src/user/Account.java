package user;

import interfaces.IAccount;
import tools.Hash;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Objects;

public class Account implements IAccount {

    private final String username;
    private final Date creation;
    private final long id;
    private String password;
    private boolean login;
    private boolean locked;
    private Date modified;

    public Account(String username, String password) {
        Objects.requireNonNull(username, "Úsername can't be NULL!");
        Objects.requireNonNull(password, "Password can't be NULL!");

        this.id = new SecureRandom().nextLong();
        this.username = username;
        this.password = Hash.hash_hex(password, id);
        this.login = false;
        this.locked = false;
        this.creation = new Date();
        this.modified = new Date();
    }

    private Account(long id, String username, String password, boolean login, boolean locked, Date creation, Date modified) {
        Objects.requireNonNull(username, "Úsername can't be NULL!");
        Objects.requireNonNull(password, "Password can't be NULL!");
        Objects.requireNonNull(creation, "Creation can't be NULL!");
        Objects.requireNonNull(modified, "Modified can't be NULL!");

        this.id = id;
        this.username = username;
        this.password = password;
        this.login = login;
        this.locked = locked;
        this.creation = creation;
        this.modified = modified;
    }

    public static IAccount loadAccount(long id, String username, String password, boolean login, boolean locked, Date creation, Date modified) {
        return new Account(id, username, password, login, locked, creation, modified);
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = Hash.hash_hex(password, this.id);
        setModified();
    }

    @Override
    public long getID() {
        return this.id;
    }

    @Override
    public boolean isLocked() {
        return this.locked;
    }

    @Override
    public void setLocked(boolean locked) {
        this.locked = locked;
        setModified();
    }

    @Override
    public boolean isLogin() {
        return this.login;
    }

    @Override
    public IAccount convert() {
        return new AccountReadonly(this);
    }

    @Override
    public boolean checkPassword(String password) {
        Objects.requireNonNull(password, "Password can't be NULL!");

        String cmp_pass = Hash.hash_hex(password, this.id);

        return cmp_pass.equals(this.password);
    }

    @Override
    public void setLogin() {
        this.login = true;
        setModified();
    }

    @Override
    public void setLogout() {
        this.login = false;
        setModified();
    }

    @Override
    public Date getCreation() {
        return creation;
    }

    @Override
    public Date getModified() {
        return modified;
    }

    private void setModified() {
        this.modified = new Date();
    }
}
