package Servelets;

import interfaces.IAccount;
import interfaces.IAuthenticator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Logger;

public class Logout extends HttpServlet {

    private final Logger LOG = Logger.getAnonymousLogger();

    public void init() throws ServletException {
        super.init();
    }

    public void destroy() {
        // release resources here
        super.destroy();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        IAccount authUser = IAuthenticator.login(request, response);
        IAuthenticator.logout(authUser);
        HttpSession session = request.getSession(false);

        if (session != null) {
            session.invalidate();
        }

    }
}
