import interfaces.IAccount;
import interfaces.IAuthenticator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

public class HelloServlet extends HttpServlet {

    private final Logger LOG = Logger.getAnonymousLogger();

    public void init() throws ServletException {
        super.init();
    }

    public void destroy() {
        // release resources here
        super.destroy();
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();


        IAccount authUser = IAuthenticator.login(req, res);

        LOG.info("authenticated " + authUser.getUsername());
        // continue with authenticated user
        // job


        try {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Hello, World</title></head>");
            out.println("<body>");
            out.println("<h1>Hello, world!</h1>");
            out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
            out.println("<p>Protocol: " + req.getProtocol() + "</p>");
            out.println("<p>PathInfo: " + req.getPathInfo() + "</p>");
            out.println("<p>Remote Address: " + req.getRemoteAddr() + "</p>");
            out.println("<p>A Random Number: <strong>" + Math.random() + "</strong></p>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
