package tools;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Hash {

    private static final String ALGO = "AES";
    private static final byte[] keyValue = new byte[]{'F', 'C', 'T', '/', 'U', 'N', 'L', 'r', 'o', 'c', 'k', 's', '!', '!', 'd', 'i'};
    private Key apiKey = new SecretKeySpec(keyValue, ALGO);

    /*
    Injective function
    if H(x) == H(y) ==> x == y
     */
    public static byte[] hash_bytes(String val, long salt) {
        MessageDigest digest;

        try {
            //TODO salt dependent on crating date of account "dynamic"
            digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(val.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    public static String hash_hex(String val, long salt) {

        return bytesToHex(hash_bytes(val, salt));
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public String encrypt(String Data) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, apiKey);
        byte[] encVal = c.doFinal(Data.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }

    public String decrypt(String encrypted) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, apiKey);
        byte[] decodedValue = Base64.getDecoder().decode(encrypted);
        byte[] decValue = c.doFinal(decodedValue);
        return new String(decValue);
    }

    private String createJWT(String id, String issuer, String subject) {
        //The JWT signature algorithm used to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        //sign JWT with ApiKey secret
        byte[] apiKeySecretBytes = Base64.getDecoder().decode(apiKey.getEncoded());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        //Set the JWT Claims
        JwtBuilder builder = Jwts.builder()
                .setId(id)
                .setSubject(subject
                ).setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);
        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    private void parseJWT(String jwt) {
        Claims claims = Jwts.parser()
                .setSigningKey(Base64.getDecoder().decode(apiKey.getEncoded()))
                .parseClaimsJws(jwt).getBody();
        System.out.println("ID: " + claims.getId());
        System.out.println("Subject: " + claims.getSubject());
        System.out.println("Issuer: " + claims.getIssuer());
    }
}
