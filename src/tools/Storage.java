package tools;

import database.SQLite;
import exceptions.AccountNotFound;
import interfaces.IAccount;
import user.Account;

import java.util.Date;

public class Storage {

    private static final SQLite sq;

    static {
        sq = new SQLite();
    }

    public static void updateAccount(IAccount account) {
        sq.update(account.getUsername(),
                account.getPassword(),
                account.isLogin(),
                account.isLocked());
    }

    public static void saveAccount(IAccount account) {
        sq.insertAccount(account.getUsername(),
                account.getPassword(),
                account.isLogin(),
                account.isLocked());
    }

    public static IAccount getAccount(String username) throws AccountNotFound {
        SQLite.Values val = sq.getAccount(username);
        //TODO val != null
        return Account.loadAccount(0, val.username,
                val.password, val.login, val.locked, new Date(), new Date());
    }

    public static void deleteAccount(String username) {
        sq.deleteAccount(username);
    }

    public static boolean hasAccount(String username) {
        return sq.getAccount(username) != null;
    }


}
